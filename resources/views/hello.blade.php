<h1>Hello World from Page Controller!</h1>
<h2>{{-- $name --}}</h2>
<h2>{{ $front_end }}</h2>

@if(count($topics) > 0)
    @foreach($topics as $topic)
        <li>{{ $topic }}</li>
    @endforeach
    @else
        <p>Nothing to display</p>
@endif